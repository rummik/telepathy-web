Telepathy Web [![Builds][]][travis] [![Deps][]][gemnasium] [![Donations][]][gittip]
=============
A telepathic password manager for browsers.

[Builds]: http://img.shields.io/travis-ci/rummik/telepathy-web.png "Build Status"
[travis]: https://travis-ci.org/rummik/telepathy-web
[Deps]: https://gemnasium.com/rummik/telepathy-web.png "Dependency Status"
[gemnasium]: https://gemnasium.com/rummik/telepathy-web
[Donations]: http://img.shields.io/gittip/rummik.png
[gittip]: https://www.gittip.com/rummik/


## Contributing
Please see the [Chameleoid Styleguide][] before contributing.

Take care to maintain the existing coding style.  Add unit tests for any new or
changed functionality.  Lint and test your code using [Grunt][].

[Chameleoid Styleguide]: https://github.com/chameleoid/style
[Grunt]: http://gruntjs.com/


## License
Copyright (c) 2013
Licensed under the MPL license.
